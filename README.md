# cordova-file-helper-legacy

Warning: This wrapper use native Promise and do NOT use `async`/`await` keywords to improve performance.
If you have access to those keywords in your target devices, use [cordova-file-helper](https://gitlab.com/Alkihis/cordova-file-helper).

Full documentation available at [cordova-file-helper](https://gitlab.com/Alkihis/cordova-file-helper).
